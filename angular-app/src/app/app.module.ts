import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchComponent } from './modules/search/search.component';
import { CreateShopComponent } from './modules/create-shop/create-shop.component';
import { AppLayoutComponent } from './layout/app-layout/app-layout.component';
import { AppHeaderComponent } from './layout/app-header/app-header.component';
import { NavComponent } from './layout/nav/nav.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    CreateShopComponent,
    AppLayoutComponent,
    AppHeaderComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBVkLU8wlmyg5Gm7WvL5gfAi10B9U7peEg',
      libraries: ['places']
    }),
    BrowserAnimationsModule, 
    ToastrModule.forRoot({
      timeOut:10000,
      positionClass: 'toast-top-right',
      preventDuplicates:false
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }