import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  
  navItems = [
    { link: '/search', title: 'Search', icon: 'fa-search' },
    { link: '/create', title: 'Create', icon: 'fa-shopping-basket' }
  ];

  
  constructor() { }

  ngOnInit() {
  }

}
