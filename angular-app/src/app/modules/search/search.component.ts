import { Component, OnInit } from '@angular/core';
import { ShopService } from '../../service/shop.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  latitude        : number = 0;
  longitude       : number = 0;
  searchTerm      : string = '';

  shops           = [];
 
  constructor(
    private shopService : ShopService
  ) { }

  ngOnInit() {
    this.setCurrentLocation();
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
      });
    }
  }

  /**
   * Search shops based on the search keyword and current location latitude and longitude 
   */
  public search() {
    this.shopService.search(this.latitude, this.longitude, this.searchTerm)
      .subscribe(
        (data) => {
          console.log(data);
          this.shops = data;
        }
      );
  }
}
