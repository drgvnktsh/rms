import { Component, OnInit,ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CategoryService } from '../../service/category.service';
import { MapsAPILoader } from '@agm/core';
import { ShopService } from '../../service/shop.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-shop',
  templateUrl: './create-shop.component.html',
  styleUrls: ['./create-shop.component.css']
})
export class CreateShopComponent implements OnInit {
  shopFormGroup   : FormGroup;

  latitude        : number;
  longitude       : number;
  zoom            : number;

  shopAddress     : string;
  address         : string;
  isFormSubmitted : boolean = false;
  
  categories = [];


  @ViewChild('search')
  public searchElementRef: ElementRef;
 

  constructor(
    private formBuilder     : FormBuilder,
    private categoryService : CategoryService,
    private shopService     : ShopService,
    private mapsAPILoader   : MapsAPILoader,
    private ngZone          : NgZone,
    private toastrService   : ToastrService
  ) { }

  ngOnInit() {
    this.setCurrentLocation();

    this.categoryService.getCategories()
      .subscribe(
        (data) => {
          this.categories = data;
        }
      );

    // building form  
    this.shopFormGroup = this.formBuilder.group({
      name      : ['', Validators.required],
      category  : ['', Validators.required],
      location  : ['', Validators.required],
      owner     : ['', Validators.required],
      latitude  : [''],
      longitude : [''],
      address : ['']
    });

    // Gooogle Maps integration
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });

      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
 
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
 
          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
          this.shopAddress = place.formatted_address;

          this.shopFormGroup.controls.location.setValue(place.formatted_address);
        });
      });

    });

  }

  /**
   * Submit shop form to save shop details  
   */
  public submitForm() {
    this.isFormSubmitted = true;
    if(this.isFormSubmitted && this.shopFormGroup.valid) {
      let formAttrs = this.shopFormGroup.controls;
  
      let name = formAttrs.name.value;

      formAttrs.latitude.setValue(this.latitude);
      formAttrs.longitude.setValue(this.longitude);
      formAttrs.address.setValue(this.shopAddress);

      this.shopService.save(this.shopFormGroup.value)
        .subscribe(
          data => {
            this.resetForm();
            this.toastrService.success("Shop "+ name +" saved succesfully");
          },
          error => {
            this.toastrService.error("Error in saving Shop Details")
          }
        );
    }
    
  }

  /**
   * reset form clears the form input fields 
   */
  public resetForm() {
    this.isFormSubmitted = false;
    this.searchElementRef.nativeElement.value = "";
    this.shopFormGroup.reset();
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
      });
    }
  }

}
