import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppLayoutComponent } from './layout/app-layout/app-layout.component';
import { SearchComponent } from './modules/search/search.component';
import { CreateShopComponent } from './modules/create-shop/create-shop.component';


const routes: Routes = [    
  //Site routes goes here 
  { 
      path: '', 
      component: AppLayoutComponent,
      children: [
        { path: '', component: SearchComponent, pathMatch: 'full'},
        { path: 'search', component: SearchComponent, data: {'pageTitle': 'Search'} },
        { path: 'create', component: CreateShopComponent, data: {'pageTitle': 'Create'} }
      ]
  },
  
  // otherwise redirect to search
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
