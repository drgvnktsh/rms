import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  baseUrl : string = environment.apiUrl + '/category/';

  constructor(
    private http : HttpClient
  ) { }

  /**
   * getCategories method is used to get list of categories
   */
  public getCategories() : Observable<any> {
    return this.http.get(this.baseUrl + 'list');
  }
}
