import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShopService {
  baseUrl : string = environment.apiUrl + '/shop/';

  constructor(
    private http : HttpClient
  ) { }

  /**
   * save method is used to save shop information
   * @param shopDtls 
   */
  public save(shopDtls) : Observable<any> {
    return this.http.post(this.baseUrl + 'save', shopDtls);
  }
  /**
   * search method id used to search the shop with the given criteria
   * @param latitude 
   * @param longitude 
   * @param searchTerm 
   */
  public search(latitude : number, longitude : number, searchTerm : string) : Observable<any> {
    let params = new HttpParams()
                  .set('latitude', latitude.toString())
                  .set('longitude', longitude.toString())
                  .set('searchTerm', searchTerm);
    return this.http.get(this.baseUrl + 'search', {params});
  }
}
