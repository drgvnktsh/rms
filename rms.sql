# categories
DROP TABLE IF EXISTS categories;
CREATE TABLE categories(
	id					INT AUTO_INCREMENT PRIMARY KEY
	,code				VARCHAR(50) UNIQUE KEY
	,display_name     	VARCHAR(250)
	,is_active			TINYINT(1) DEFAULT '1'	
);

# shops
DROP TABLE IF EXISTS shops;
CREATE TABLE shops(
	id					INT AUTO_INCREMENT PRIMARY KEY
	,name       		VARCHAR(250)
	,category_id		INT(32)
	,latitude     		DECIMAL(10,8)
	,longitude   		DECIMAL(11,8)
	,owner       		VARCHAR(250)
	,address			VARCHAR(250)
	,is_active			TINYINT(1) DEFAULT '1'
	,date_created 		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
	,last_updated 		TIMESTAMP NULL DEFAULT NULL	
);

			
			
INSERT INTO categories(code, display_name)
	VALUES ('GS', 'General Store'),
			('ML', 'Mall'),
			('SM', 'Super Market'),
			('MS', 'Medical Store');			


-- -------------------------------------------------------------------------------

-- Stored Procedure : To get the shops near by latitude & longitude with keyword search

-- @param	: latitude
-- @param	: longitude
-- @param	: searchTerm

-- -------------------------------------------------------------------------------
			
			
DROP PROCEDURE IF EXISTS GetShops;

DELIMITER $$

CREATE PROCEDURE GetShops(latitude DECIMAL(10, 8), longitude DECIMAL(11, 8), searchTerm VARCHAR(250))
BEGIN
	
	SELECT s.id
		,s.name
		,c.display_name
		,s.latitude
		,s.longitude
		,s.owner
		,s.address
		,( 3959 * acos (
		  cos ( radians(latitude) ) * cos( radians( s.latitude ) )
		  * cos( radians( s.longitude ) - radians(longitude) )
		  + sin ( radians(latitude) )
		  * sin( radians( s.latitude ) )
		)
		) AS distance
	FROM shops s
	INNER JOIN categories c ON c.id = s.category_id
	WHERE s.name LIKE CONCAT('%', searchTerm ,'%')
	HAVING distance < 30;

END
	