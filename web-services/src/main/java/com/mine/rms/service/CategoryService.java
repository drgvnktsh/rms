/**
 * CategoryService interface to define abstract methods to manipulate the Category object data.
 * 
 * @author Durga Venkatesh Sambhani
 * 
 * */
package com.mine.rms.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mine.rms.model.Category;

@Component
public interface CategoryService {
	/**
     * Returns list of Categories
     * 
     * @return - List of categories
     */
	public List<Category> getCategories();
}
