/**
 * CategoryService is used to manipulate the Category object data.
 * 
 * @author Durga Venkatesh Sambhani
 * 
 * */
package com.mine.rms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mine.rms.model.Category;
import com.mine.rms.repository.CategoryRepositiory;
import com.mine.rms.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {
	@Autowired
	CategoryRepositiory categoryRepository;
	
	/**
     * Returns list of Categories
     * 
     * @return - List of categories
     */
	
	@Override
	public List<Category> getCategories() {
		List<Category> categories = categoryRepository.findAllByIsActive(true);
		
		return categories;
	}

}
