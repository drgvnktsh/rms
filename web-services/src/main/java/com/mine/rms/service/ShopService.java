/**
 * ShopService interface to define abstract methods to  the Shop object data.
 * 
 * @author Durga Venkatesh Sambhani
 * 
 * */
package com.mine.rms.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Component;

import com.mine.rms.model.Shop;

@Component
public interface ShopService {
	/**
     * Returns list of shops which are near by given latitude, longitude and search keyword
     * 
     * @param latitude - Latitude
     * @param Longitude - Longitude
     * @param searchTerm - keyword
     * @return - List of shops for the given matched criteria
     */
	public List<Shop> search(BigDecimal latitude, BigDecimal longitude, String searchTerm);
	
	/**
     * Saves shop details
     * 
     * @param shop - shop object
     * @return - saved shop information
     */
	public Shop save(Shop shop);

}
