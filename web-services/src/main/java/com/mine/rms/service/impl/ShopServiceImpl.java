/**
 * ShopService is used to manipulate the Shop object data.
 * 
 * @author Durga Venkatesh Sambhani
 * 
 * */
package com.mine.rms.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mine.rms.model.Shop;
import com.mine.rms.repository.ShopRepository;
import com.mine.rms.service.ShopService;

@Service
public class ShopServiceImpl implements ShopService {
	
	@Autowired
	ShopRepository shopRepository;
	
	@Autowired
	EntityManager em;
	
	/**
     * Returns list of shops which are near by given latitude, longitude and search keyword
     * 
     * @param latitude - Latitude
     * @param Longitude - Longitude
     * @param searchTerm - keyword
     * @return - List of shops for the given matched criteria
     */
	@Override
	public List<Shop> search(BigDecimal latitude, BigDecimal longitude, String searchTerm) {
	
		List<Object[]> result = getShops(latitude, longitude, searchTerm);
				
		List<Shop> shops = new ArrayList();
		 
		for(Object[] obj : result) {
			Shop shop = new Shop();

			shop.setName((String)obj[1]);
			shop.setCategory((String)obj[2]);
			shop.setLatitude((BigDecimal)obj[3]);
			shop.setLongitude((BigDecimal)obj[4]);
			shop.setOwner((String)obj[5]);
			shop.setAddress((String)obj[6]);
			
			shops.add(shop);
		} 
		
		return shops;
	}
	
	/**
     * Saves shop details
     * 
     * @param shop - shop object
     * @return - saved shop information
     */
	@Override
	public Shop save(Shop shop) {
		return shopRepository.save(shop);
	}
	
	private List<Object[]> getShops(BigDecimal latitude, BigDecimal longitude, String searchTerm) {
		 List<Object[]> result = em
			      .createNativeQuery("call GetShops(:latitude,:longitude, :searchTerm)")
			      .setParameter("latitude", latitude)
			      .setParameter("longitude", longitude)
			      .setParameter("searchTerm", searchTerm)
			      .getResultList();
		
		 return result;
	}

}
