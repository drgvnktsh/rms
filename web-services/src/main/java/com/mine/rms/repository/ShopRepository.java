package com.mine.rms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mine.rms.model.Shop;

public interface ShopRepository extends JpaRepository<Shop, Long> {
	
}
