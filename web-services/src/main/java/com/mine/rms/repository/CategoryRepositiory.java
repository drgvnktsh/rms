package com.mine.rms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mine.rms.model.Category;

public interface CategoryRepositiory extends JpaRepository<Category, Long> {
	List<Category> findAllByIsActive(Boolean isActive);
}
