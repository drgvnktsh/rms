/**
* CategoryController is used to handle Category Object requests
* 
* @author Durga Venkatesh Sambhani
* 
*/
package com.mine.rms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mine.rms.model.Category;
import com.mine.rms.service.CategoryService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/category")
public class CategoryController {
	@Autowired
	CategoryService categoryService;
	
	/**
     * Returns list of categories
     * 
     * @return - categories
     */
	
	@GetMapping("list")
	public List<Category> list() {
		List<Category> list = categoryService.getCategories();
		
		return list;
	}
}
