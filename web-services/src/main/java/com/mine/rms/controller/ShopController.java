/**
* ShopController is used to handle Shop Object requests
* 
* @author Durga Venkatesh Sambhani
* 
*/

package com.mine.rms.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mine.rms.model.Shop;
import com.mine.rms.service.ShopService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/shop")
public class ShopController {

	@Autowired
	ShopService shopService;
	
	/**
     * Returns list of shops which are near by given latitude, longitude and search keyword
     * 
     * @param latitude - Latitude
     * @param Longitude - Longitude
     * @param searchTerm - keyword
     * @return - List of shops for the given matched criteria
     */
	@GetMapping("/search")
	public List<Shop> search(@RequestParam BigDecimal latitude, @RequestParam BigDecimal longitude, @RequestParam String searchTerm) {
		List<Shop> shops = shopService.search(latitude, longitude, searchTerm);
		
		return shops;
	}
	
	/**
     * Saves shop details
     * 
     * @param shop - shop object
     * @return - saved shop information
     */
	@PostMapping("/save")
	public Shop save(@RequestBody Shop shop) {
		System.out.println(shop);
		
		return shopService.save(shop);
	}
}
