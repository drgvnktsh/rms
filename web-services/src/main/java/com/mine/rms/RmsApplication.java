package com.mine.rms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
@RestController
@ComponentScan("com.mine.rms") 
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RmsApplication {
	
	@RequestMapping(value="/")
    String hello() {
        return "Hello Welcome to Retail Management System";
    }

	public static void main(String[] args) {
		SpringApplication.run(RmsApplication.class, args);
	}

}
